﻿#include <iostream>

void func(int limit, bool OddOrEvenNumbers)
{
	for (int i = OddOrEvenNumbers % 2; i < limit; i += 2)
	{
		std::cout << i;
		std::cout << "\n";
	}
}
int main()
{
	func(20, false);

	return 0;
}